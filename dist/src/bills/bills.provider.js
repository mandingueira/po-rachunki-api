"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bill_schema_1 = require("./schemas/bill.schema");
exports.billsProviders = [
    {
        provide: 'BillModelToken',
        useFactory: (connection) => connection.model('Bill', bill_schema_1.BillSchema),
        inject: ['DbConnectionToken'],
    },
];
//# sourceMappingURL=bills.provider.js.map