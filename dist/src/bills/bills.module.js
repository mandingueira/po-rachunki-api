"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const bills_controller_1 = require("./bills.controller");
const bills_service_1 = require("./bills.service");
const bills_provider_1 = require("./bills.provider");
const database_module_1 = require("../database/database.module");
const passport_1 = require("@nestjs/passport");
exports.passportModule = passport_1.PassportModule.register({ defaultStrategy: 'jwt' });
let BillsModule = class BillsModule {
};
BillsModule = __decorate([
    common_1.Module({
        imports: [database_module_1.DatabaseModule, exports.passportModule,
            common_1.MulterModule.register({
                dest: './uploads',
            }),
        ],
        controllers: [bills_controller_1.BillsController],
        providers: [bills_service_1.BillsService,
            ...bills_provider_1.billsProviders],
    })
], BillsModule);
exports.BillsModule = BillsModule;
//# sourceMappingURL=bills.module.js.map