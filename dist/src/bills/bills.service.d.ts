import { Model } from 'mongoose';
import { CreateBillDto } from './dto/create-bill.dto';
import { Bill } from './interfaces/bill.interfaces';
export declare class BillsService {
    private readonly billModel;
    constructor(billModel: Model<Bill>);
    create(createBillDto: CreateBillDto): Promise<Bill>;
    findAll(): Promise<Bill[]>;
    findAllByUser(createdById: string): Promise<Bill[]>;
    find(purchaseType: string): Promise<any>;
    findOneById(id: string): Promise<any>;
    updateBill(id: any, createBillDto: CreateBillDto): Promise<Bill>;
    destroyById(id: string): Promise<{
        deleted: boolean;
    }>;
    filter(data: string, currentUser: string): Promise<any[]>;
    filterAll(filters: any, currentUser: string): Promise<any>;
}
