"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
const mongoose_1 = require("mongoose");
const common_1 = require("@nestjs/common");
const mongodb_1 = require("mongodb");
let BillsService = class BillsService {
    constructor(billModel) {
        this.billModel = billModel;
    }
    create(createBillDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const createdBill = new this.billModel(createBillDto);
            return yield createdBill.save();
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.billModel.find().exec();
        });
    }
    findAllByUser(createdById) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.billModel.find({ createdById }).exec();
        });
    }
    find(purchaseType) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.billModel.find({ purchaseType });
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.billModel.findById(id);
        });
    }
    updateBill(id, createBillDto) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.billModel.findByIdAndUpdate(id, createBillDto, { new: true });
        });
    }
    destroyById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.billModel.findByIdAndDelete(id);
            return { deleted: true };
        });
    }
    filter(data, currentUser) {
        return __awaiter(this, void 0, void 0, function* () {
            const filterList = [];
            const products = yield this.billModel.aggregate([
                { $match: {
                        $and: [
                            { createdById: currentUser },
                            { product: { $regex: data, $options: 'i' },
                            }
                        ]
                    } },
                { $group: {
                        _id: '$product',
                        idList: { $push: '$_id' },
                    } },
            ]).exec();
            const brands = yield this.billModel.aggregate([
                { $match: {
                        createdById: currentUser,
                        brand: { $regex: data, $options: 'i' },
                    } },
                { $group: {
                        _id: '$brand',
                        idList: { $push: '$_id' },
                    } },
            ]).exec();
            const shops = yield this.billModel.aggregate([
                { $match: {
                        createdById: currentUser,
                        shop: { $regex: data, $options: 'i' },
                    } },
                { $group: {
                        _id: '$shop',
                        idList: { $push: '$_id' },
                    } },
            ]).exec();
            filterList.push(...products, ...brands, ...shops);
            return yield filterList;
        });
    }
    filterAll(filters, currentUser) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(filters);
            const filterParse = JSON.parse(JSON.stringify(filters));
            const idList = filters.searchIdList.map(id => mongodb_1.ObjectId(id));
            const filterArray = [
                { createdById: currentUser },
            ];
            if (filters.selectedCategory.length) {
                filterArray.push({ purchaseType: { $in: filterParse.selectedCategory } });
            }
            if (filters.selectedPriceFrom) {
                filterArray.push({ price: { $gt: filterParse.selectedPriceFrom } });
            }
            if (filters.selectedPriceTo) {
                filterArray.push({ price: { $lte: filterParse.selectedPriceTo } });
            }
            if (filters.purchaseDateFrom) {
                filterArray.push({ purchaseDate: { $gt: filterParse.purchaseDateFrom } });
            }
            if (filters.purchaseDateTo) {
                filterArray.push({ purchaseDate: { $lte: filterParse.purchaseDateTo } });
            }
            if (filters.warrantyDateFrom) {
                filterArray.push({ warrantyEndDate: { $gt: filterParse.warrantyDateFrom } });
            }
            if (filters.warrantyDateTo) {
                filterArray.push({ warrantyEndDate: { $lte: filterParse.warrantyDateTo } });
            }
            if (idList.length) {
                filterArray.push({ _id: { $in: idList } });
            }
            const billsFiltered = yield this.billModel.find({
                $and: filterArray
            });
            return yield billsFiltered;
        });
    }
};
BillsService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject('BillModelToken')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_1.Model !== "undefined" && mongoose_1.Model) === "function" ? _a : Object])
], BillsService);
exports.BillsService = BillsService;
//# sourceMappingURL=bills.service.js.map