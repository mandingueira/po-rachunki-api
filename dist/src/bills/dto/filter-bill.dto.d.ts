export declare class FilterBillDto {
    readonly createdById?: string;
    readonly selectedCategory?: string[];
    readonly selectedPriceFrom?: number;
    readonly selectedPriceTo?: number;
    readonly purchaseDateFrom?: string;
    readonly purchaseDateTo?: string;
    readonly warrantyDateFrom?: string;
    readonly warrantyDateTo?: string;
    readonly searchIdList?: string[];
}
