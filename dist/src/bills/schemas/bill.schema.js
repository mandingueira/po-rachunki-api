"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.BillSchema = new mongoose.Schema({
    imageBillPath: String,
    imageProductPath: String,
    price: Number,
    purchaseDate: String,
    purchaseType: String,
    shop: String,
    product: [String],
    brand: [String],
    warranty: Number,
    warrantyEndDate: String,
    description: String,
    createdAt: String,
    updatedAt: String,
    createdById: mongoose.Schema.Types.ObjectId,
    updatedById: mongoose.Schema.Types.ObjectId,
});
//# sourceMappingURL=bill.schema.js.map