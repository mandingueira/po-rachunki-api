"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const bills_service_1 = require("./bills.service");
const create_bill_dto_1 = require("./dto/create-bill.dto");
const swagger_1 = require("@nestjs/swagger");
const multer_config_1 = require("../config/multer.config");
const user_decorator_1 = require("../users/user.decorator");
const passport_1 = require("@nestjs/passport");
const filter_bill_dto_1 = require("./dto/filter-bill.dto");
let BillsController = class BillsController {
    constructor(billsServices) {
        this.billsServices = billsServices;
    }
    create(createBillDto) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.create(createBillDto);
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.findAll();
        });
    }
    findBillsForUser(user) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(user);
            return this.billsServices.findAllByUser(user.userId);
        });
    }
    findOne(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.find(data);
        });
    }
    findOneById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.findOneById(id);
        });
    }
    updateCustomer(res, id, createBillDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const bill = yield this.billsServices.updateBill(id, createBillDto);
            if (!bill) {
                throw new common_1.NotFoundException('Cat does not exist!');
            }
            return res.status(common_1.HttpStatus.OK).json({
                message: 'Bill has been successfully updated',
                bill,
            });
        });
    }
    removeById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.destroyById(id);
        });
    }
    filter(user, data) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.filter(data, user.userId);
        });
    }
    filterAll(filterBillDto, user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.billsServices.filterAll(filterBillDto, user.userId);
        });
    }
    logFiles(images, fileDto) {
        console.log(images);
        console.log(fileDto);
        return images;
    }
    seeUploadedFile(image, res) {
        return res.sendFile(image, { root: 'uploads' });
    }
};
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Post('create'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_bill_dto_1.CreateBillDto]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "create", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('allBills'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "findAll", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('allUserBills'),
    __param(0, user_decorator_1.User()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "findBillsForUser", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('findByPurchaseType/:purchaseType'),
    __param(0, common_1.Param('purchaseType')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "findOne", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('find/:id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "findOneById", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Put('/update'),
    __param(0, common_1.Res()), __param(1, common_1.Query('id')), __param(2, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, create_bill_dto_1.CreateBillDto]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "updateCustomer", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Delete('delete/:id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "removeById", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('filter/:data'),
    __param(0, user_decorator_1.User()), __param(1, common_1.Param('data')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "filter", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Post('filterAll'),
    __param(0, common_1.Body()), __param(1, user_decorator_1.User()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [filter_bill_dto_1.FilterBillDto, Object]),
    __metadata("design:returntype", Promise)
], BillsController.prototype, "filterAll", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Post('/uploadPhoto'),
    common_1.UseInterceptors(common_1.FilesInterceptor('images', 20, multer_config_1.multerOptions)),
    __param(0, common_1.UploadedFiles()), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], BillsController.prototype, "logFiles", null);
__decorate([
    common_1.Get(':imgpath'),
    __param(0, common_1.Param('imgpath')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], BillsController.prototype, "seeUploadedFile", null);
BillsController = __decorate([
    swagger_1.ApiUseTags('bills'),
    common_1.Controller('bills'),
    __metadata("design:paramtypes", [bills_service_1.BillsService])
], BillsController);
exports.BillsController = BillsController;
//# sourceMappingURL=bills.controller.js.map