export declare const billsProviders: {
    provide: string;
    useFactory: (connection: any) => any;
    inject: string[];
}[];
