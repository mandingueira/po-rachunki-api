import { BillsService } from './bills.service';
import { Bill } from './interfaces/bill.interfaces';
import { CreateBillDto } from './dto/create-bill.dto';
import { FilterBillDto } from './dto/filter-bill.dto';
export declare class BillsController {
    private readonly billsServices;
    constructor(billsServices: BillsService);
    create(createBillDto: CreateBillDto): Promise<Bill>;
    findAll(): Promise<Bill[]>;
    findBillsForUser(user: any): Promise<Bill[]>;
    findOne(data: string): Promise<Bill>;
    findOneById(id: string): Promise<Bill>;
    updateCustomer(res: any, id: string, createBillDto: CreateBillDto): Promise<any>;
    removeById(id: string): Promise<{
        deleted: boolean;
    }>;
    filter(user: any, data: string): Promise<any[]>;
    filterAll(filterBillDto: FilterBillDto, user: any): Promise<Bill[]>;
    logFiles(images: any, fileDto: any): any;
    seeUploadedFile(image: any, res: any): any;
}
