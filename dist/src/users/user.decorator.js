"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
exports.User = common_1.createParamDecorator((options = {}, req) => {
    const user = req.user;
    if (options.required && !user) {
        throw new common_1.UnauthorizedException();
    }
    return user;
});
//# sourceMappingURL=user.decorator.js.map