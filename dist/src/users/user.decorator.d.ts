export interface CurrentUserOptions {
    required?: boolean;
}
export declare const User: (options?: CurrentUserOptions) => ParameterDecorator;
