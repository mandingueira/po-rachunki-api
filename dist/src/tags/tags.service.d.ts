import { Model } from 'mongoose';
import { Tag } from './interfaces/tag.interfaces';
import { CreateTagDto } from './dto/create-tag.dto';
export declare class TagsService {
    private readonly tagModel;
    constructor(tagModel: Model<Tag>);
    create(createTagDto: CreateTagDto): Promise<Tag>;
    findAll(createdById: string): Promise<Tag[]>;
    findBasic(createdById: string): Promise<Tag[]>;
    destroyById(id: string): Promise<{
        deleted: boolean;
    }>;
}
