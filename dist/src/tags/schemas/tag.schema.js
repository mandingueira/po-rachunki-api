"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.TagSchema = new mongoose.Schema({
    label: String,
    type: String,
    belongToLabel: [String],
    createdById: String,
});
//# sourceMappingURL=tag.schema.js.map