"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tag_schema_1 = require("./schemas/tag.schema");
exports.tagsProviders = [
    {
        provide: 'TagModelToken',
        useFactory: (connection) => connection.model('Tag', tag_schema_1.TagSchema),
        inject: ['DbConnectionToken'],
    },
];
//# sourceMappingURL=tags.provider.js.map