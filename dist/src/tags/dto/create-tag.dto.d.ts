export declare class CreateTagDto {
    readonly label: string;
    readonly type: string;
    readonly belongToLabel?: string[];
    readonly createdById?: string[];
}
