export declare const tagsProviders: {
    provide: string;
    useFactory: (connection: any) => any;
    inject: string[];
}[];
