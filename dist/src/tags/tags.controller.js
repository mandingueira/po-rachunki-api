"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const tags_service_1 = require("./tags.service");
const create_tag_dto_1 = require("./dto/create-tag.dto");
const swagger_1 = require("@nestjs/swagger");
const user_decorator_1 = require("../users/user.decorator");
const passport_1 = require("@nestjs/passport");
let TagsController = class TagsController {
    constructor(tagsServices) {
        this.tagsServices = tagsServices;
    }
    create(createTagDto) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.tagsServices.create(createTagDto);
        });
    }
    findAll(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.tagsServices.findAll(user.userId);
        });
    }
    findBasic() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.tagsServices.findBasic('basic');
        });
    }
    removeById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.tagsServices.destroyById(id);
        });
    }
};
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Post('create'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_tag_dto_1.CreateTagDto]),
    __metadata("design:returntype", Promise)
], TagsController.prototype, "create", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('userTags'),
    __param(0, user_decorator_1.User()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], TagsController.prototype, "findAll", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Get('basic'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], TagsController.prototype, "findBasic", null);
__decorate([
    common_1.UseGuards(passport_1.AuthGuard()),
    common_1.Delete('delete/:id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], TagsController.prototype, "removeById", null);
TagsController = __decorate([
    swagger_1.ApiUseTags('tags'),
    common_1.Controller('tags'),
    __metadata("design:paramtypes", [tags_service_1.TagsService])
], TagsController);
exports.TagsController = TagsController;
//# sourceMappingURL=tags.controller.js.map