import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { Tag } from './interfaces/tag.interfaces';
export declare class TagsController {
    private readonly tagsServices;
    constructor(tagsServices: TagsService);
    create(createTagDto: CreateTagDto): Promise<Tag>;
    findAll(user: any): Promise<Tag[]>;
    findBasic(): Promise<Tag[]>;
    removeById(id: string): Promise<{
        deleted: boolean;
    }>;
}
